%{
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include <unistd.h>
	
	#include "fibo/fibo.h"

	extern int yylex();
	extern FILE *yyin;

	void display(inst_operation_t *start, int displayType);
	void yyerror(char const *s);

	enum {
		DISP_KEY_POINT, 
		DISP_KEY_COULEUR, 
		DISP_KEY_TITRE, 
		DISP_KEY_EPAISSEUR, 
		DISP_KEY_DEBUG_INST, 
		DISP_KEY_DEBUG_MEMO, 
		DISP_KEY_FORMAT, 
		NB_DISP_KEY 
		};

	enum {
		DISP_TYPE_TEXT, 
		DISP_TYPE_GRAPH
		};

	memo_vars_t *memory = NULL;
	fonction_list_t *fonctions = NULL;
	inst_operation_t* displayOperation[NB_DISP_KEY];
	memo_valeur_t* displayValue[NB_DISP_KEY];

	
%}

%error-verbose

%union {
	int chiffres;
	int boolean;
	char *nom;
	char *chaine;
	char chr;

	struct inst_operation_t *type_inst_operation;
	struct inst_valeur_t *type_inst_valeur;
	struct inst_tableau_t *type_inst_tableau;
	struct inst_variable_t *type_inst_variable;
	struct inst_index_t *type_inst_index;
	struct inst_arguments_t *type_inst_arguments;
	struct inst_code_t *type_inst_code;
} 

%start G

%token <chiffres> 	Chiffres
%token <nom> 		Nom
%token <chr> 		Sign
%token 				Archi
%token 				Exit
%token <chaine>		Chaine
%token 				Return
%token 				Eof
%token 				Eol
%token 				TypeReturn

%type <boolean>				End
%type <type_inst_operation> Operation
%type <type_inst_operation> Affectation
%type <type_inst_operation> Math
%type <type_inst_valeur> 	Valeur
%type <type_inst_valeur> 	Fonction_Descriptive
%type <type_inst_valeur> 	Fonction
%type <type_inst_valeur> 	Tableau
%type <type_inst_tableau> 	Tableau_Element
%type <type_inst_index> 	Index
%type <type_inst_arguments> Arguments
%type <type_inst_code> 		Code

%%
G :
	| G End											{if(!$2){printf("\n");}else{YYACCEPT;}}
	| G Exit End									{YYACCEPT;}
	| G Display Operation End 						{display($3, DISP_TYPE_GRAPH);}
	| G Operation End 								{display($2, DISP_TYPE_TEXT);}
	| G error End  									{yyerrok;}
	;

End : Eol											{$$ = 0;}
	| Eof											{$$ = 1;}
	;

Operation
	: Affectation									{$$ = $1;}
	| Math											{$$ = $1;}
	;

Affectation 
	: Nom '=' Math 									{$$ = inst_operation_affectation_init($3, inst_variable_init(NULL, $1));}
	| Nom Index '=' Math 							{$$ = inst_operation_affectation_init($4, inst_variable_init($2, $1));}
	;

Math
	: Valeur 										{$$ = inst_operation_valeur_init($1);}
	| Math Sign Valeur								{$$ = inst_operation_math_init($1, $3, $2);}
	;

Valeur
	: Fonction_Descriptive Index 					{$$ = inst_valeur_valindex_init($1, $2);}
	| Fonction Index 								{$$ = inst_valeur_valindex_init($1, $2);}
	| Nom Index 									{$$ = inst_valeur_valindex_init(inst_valeur_variable_init($1), $2);}
	| Tableau Index 								{$$ = inst_valeur_valindex_init($1, $2);}
	| Fonction_Descriptive							{$$ = $1;}
	| Fonction 										{$$ = $1;}
	| Nom 											{$$ = inst_valeur_variable_init($1);}
	| Tableau 										{$$ = $1;}
	| Chiffres 										{$$ = inst_valeur_chiffre_init($1);}
	| Chaine 										{$$ = inst_valeur_chaine_init($1);}
	;
Fonction 
	: Nom '(' Arguments ')' 						{$$ = inst_valeur_fonction_init($1, $3);}
	;

Fonction_Descriptive 
	: Nom '(' '(' Arguments ')' '=' '>' '(' Nom ')' '{' Code '}' ')' 
													{$$ = inst_valeur_fonctiondesc_init($1, $4, $9, $12);}
	;

Tableau
	: '[' Tableau_Element ']'						{$$ = inst_valeur_tableau_init($2);}
	| '[' ']'										{$$ = inst_valeur_tableau_init(NULL);}
	;

Tableau_Element
	: Operation 									{$$ = inst_tableau_init(NULL, $1);}
	| Tableau_Element ',' Operation 				{$$ = inst_tableau_init($1, $3);}
	;

Index
	: '[' Operation ']' 							{$$ = inst_index_init($2, NULL);}
	| Index '[' Operation ']' 						{$$ = inst_index_init($3, $1);}
	;


Arguments
	: Operation 									{$$ = inst_arguments_init($1, NULL);}
	| Arguments ',' Operation 						{$$ = inst_arguments_init($3, $1);}
	;

Code
	: Return Operation 								{$$ = inst_code_ret_init($2);}
	| Operation 									{$$ = inst_code_init($1);}
	;

Display
	: TypeReturn '(' Arguments ')' ':'				{/* appeler une fonction C qui change le type de retour par defaut*/}
	| TypeReturn ':'								{/* appeler une fonction C qui change le type de retour par defaut*/}
	;

%%



void display(inst_operation_t *start, int displayType)
{
	int i;
	for(i = 0; i < NB_DISP_KEY; i++) {
		if(displayOperation[i] != NULL) {
			displayValue[i] = inst_operation_exec(displayOperation[i], memory, fonctions);
		}
	}

	memo_valeur_t *value = inst_operation_exec(start, memory, fonctions);

	if(displayType == DISP_TYPE_TEXT) {
		/* === DEFAULT DISPLAY CONFIG : Configuration par defaut pour sortie dans le terminal selon le format + mode debug === */
		int conf_displayFormat = 1;
		int conf_debugInst = 0;
		int conf_debugMemo = 0;

		/* === TRAITEMENT DES VALEURS DE CONFIG === */
		if(displayValue[DISP_KEY_FORMAT] != NULL) {
			if(memo_valeur_is_str(displayValue[DISP_KEY_FORMAT])) {
				char *strFormat = memo_valeur_get_str(displayValue[DISP_KEY_FORMAT]);
				if(strcmp(strFormat, "default") == 0) {
					conf_displayFormat = 1;
				} else if(strcmp(strFormat, "json") == 0) {
					conf_displayFormat = 0;
				} else {
					fprintf(stderr, "erreur : format '%s' non défini (default/json) \n", strFormat);
					displayValue[DISP_KEY_FORMAT] = NULL;
				}
			} else {
				fprintf(stderr, "erreur : format doit être de type string\n");
				displayValue[DISP_KEY_FORMAT] = NULL;
			}
		}
		if(displayValue[DISP_KEY_DEBUG_INST] != NULL) {
			if(memo_valeur_is_int(displayValue[DISP_KEY_DEBUG_INST])) {
				conf_debugInst = memo_valeur_get_int(displayValue[DISP_KEY_DEBUG_INST]);
				if(conf_debugInst != 0) {
					conf_debugInst = 1;
				}
			} else {
				fprintf(stderr, "erreur : debugInst doit être de type int (0/1)\n");
				displayValue[DISP_KEY_DEBUG_INST] = NULL;
			}
		}
		if(displayValue[DISP_KEY_DEBUG_MEMO] != NULL) {
			if(memo_valeur_is_int(displayValue[DISP_KEY_DEBUG_MEMO])) {
				conf_debugMemo = memo_valeur_get_int(displayValue[DISP_KEY_DEBUG_MEMO]);
				if(conf_debugMemo != 0) {
					conf_debugMemo = 1;
				}
			} else {
				fprintf(stderr, "erreur : debugMemo doit être de type int (0/1)\n");
				displayValue[DISP_KEY_DEBUG_MEMO] = NULL;
			}
		}

		/* === CHOISI LE MODE D'AFFICHAGE === */

		if(conf_displayFormat == 0) {
			/* === RESULT : Récupération de la chaine de char correspondant au résultat au format json (type + valeur) === */
			char *str = memo_valeur_get_json_tree(value);
			printf("%s\n", str);
			free(str);
		} else if(conf_displayFormat == 1) {
			/* === RESULT : Récupération de la chaine de char correspondant à un tableau ou un int === */
			char *str = memo_valeur_get_value_tree(value);
			printf("%s\n", str);
			free(str);
		} 

		if(conf_debugInst == 1) {
			/* === DEBUG : Représentation JSON de la décomposition d'une instruction === */
			char *str = inst_operation_get_json_tree(start);
			printf("%s\n", str);
			free(str);
		}

		if(conf_debugMemo == 1) {
			/* === DEBUG : Représentation JSON de la mémoire (variable + contenue) === */
			char *str = memo_vars_get_json_tree(memory);
			printf("%s\n", str);
			free(str);
		}

	} else if(displayType == DISP_TYPE_GRAPH) {
		/* === DEFAULT DISPLAY CONFIG : Configuration par defaut pour un affichage sous plot === */
		char *conf_titre = "";
		char *conf_couleur = "red";
		int conf_ptStyleType = 1;
		int conf_ptStyleId = 1;
		char conf_ptStyleChar = '+';
		int conf_epaisseur = 1;

		/* === TRAITEMENT DES VALEURS DE CONFIG === */
		if(displayValue[DISP_KEY_TITRE] != NULL) {
			if(memo_valeur_is_str(displayValue[DISP_KEY_TITRE])) {
				conf_titre = memo_valeur_get_str(displayValue[DISP_KEY_TITRE]);
			} else {
				fprintf(stderr, "erreur : le titre du graphique doit être de type string\n");
				displayValue[DISP_KEY_TITRE] = NULL;
			}
		}
		if(displayValue[DISP_KEY_COULEUR] != NULL) {
			if(memo_valeur_is_str(displayValue[DISP_KEY_COULEUR])) {
				conf_couleur = memo_valeur_get_str(displayValue[DISP_KEY_COULEUR]);
			} else {
				fprintf(stderr, "erreur : la couleur doit être du type string\n");
				displayValue[DISP_KEY_COULEUR] = NULL;
			}
		}
		if(displayValue[DISP_KEY_POINT] != NULL) {
			if(memo_valeur_is_int(displayValue[DISP_KEY_POINT])) {
				conf_ptStyleId = memo_valeur_get_int(displayValue[DISP_KEY_POINT]);
				conf_ptStyleType = 0;
			} else if(memo_valeur_is_str(displayValue[DISP_KEY_POINT])) {
				char *pointStr = memo_valeur_get_str(displayValue[DISP_KEY_POINT]);
				if(strlen(pointStr) == 1) {
					conf_ptStyleChar = pointStr[0];
					conf_ptStyleType = 1;
				}  else {
					fprintf(stderr, "erreur : erreur le type de point doit être décrit que par un seul char\n");
					displayValue[DISP_KEY_POINT] = NULL;
				}
			} else {
				fprintf(stderr, "erreur : erreur le type de point doit être soit de type int soit string\n");
				displayValue[DISP_KEY_POINT] = NULL;
			}
		}
		if(displayValue[DISP_KEY_EPAISSEUR] != NULL) {
			if(memo_valeur_is_int(displayValue[DISP_KEY_EPAISSEUR])) {
				conf_epaisseur = memo_valeur_get_int(displayValue[DISP_KEY_EPAISSEUR]);
			} else {
				fprintf(stderr, "erreur : erreur l'épaisseur du trait doit être de type int\n");
				displayValue[DISP_KEY_EPAISSEUR] = NULL;
			}
		}


		/* === RESULT DATA FILE : Récupération d'un tableau de nombre et insertion dans un fichier === */
		if(memo_valeur_is_list(value)) {
			int len = memo_valeur_list_len(value);
			int i;
			FILE *output = fopen("output/data", "w");
			for(i = 0; i < len; i++) {
				memo_valeur_t *valeurCase = memo_valeur_list_get(value, i);

				if(memo_valeur_is_int(valeurCase)) {
					fprintf(output, "%ld\n", memo_valeur_get_long_int(valeurCase));
				} else {
					fprintf(stderr, "erreur : le résultat attendu est une liste contenant uniquement des nombres\n");
				}
			}
			fclose(output);
		} else {
			fprintf(stderr, "erreur : le résultat attendu doit être une liste");
		}


		/* === RESULT PLOT FILE : Récuperation d'un fichier de config === */
		FILE *configFile = fopen("output/config.plot", "w");
		fprintf(configFile, "set term png\n");
		fprintf(configFile, "set output \"output/graph.png\"\n");
		fprintf(configFile, "set title \"%s\"\n", conf_titre);
		fprintf(configFile, "plot \"output/data\" with linespoints ");
		if(conf_ptStyleType == 0) {
			fprintf(configFile, "pt %d ", conf_ptStyleId);
		} else if(conf_ptStyleType == 1) {
			fprintf(configFile, "pt \"%c\" ", conf_ptStyleChar);
		}
		fprintf(configFile, "lw %d ", conf_epaisseur);
		fprintf(configFile, "lt rgb \"%s\" ", conf_couleur);
		fprintf(configFile, "title \"\"");
		fclose(configFile);

		char *arguments[] = {"gnuplot", "output/config.plot", NULL};
		if (execv("/usr/bin/gnuplot", arguments) == -1) {
	    	perror("execv");
	    }

	}

	/* === OBLIGATOIR : liberation de la mémoire === */
	if(value != NULL) {
	 	memo_valeur_free(value);
	}


	for(i = 0; i < NB_DISP_KEY; i++) {
		if(displayOperation[i] != NULL) {
			inst_operation_free(displayOperation[i]);
		}
	}
	
	inst_operation_free(start);
}

void yyerror(char const *err)
{
	fprintf(stderr, "Compilation fail : %s\n", err);
	
}

int main(int argc, char *argv[]) {
	int i;

	memory = memo_vars_init();
	fonctions = fonction_init();

	for(i = 0; i < NB_DISP_KEY; i++) {
		displayOperation[i] = NULL;
		displayValue[i] = NULL;
	}

	if(argc == 1) {
		yyparse(); 
	} else if(argc == 2) {
		yyin = fopen(argv[1], "r");
		yyparse(); 
		fclose(yyin);
	}
	memo_vars_free(memory);
	fonction_free(fonctions);

	for(i = 0; i < NB_DISP_KEY; i++) {
		if(displayValue[i] != NULL) {
			memo_valeur_free(displayValue[i]);
		}
	}

	return 0;
}


