

struct inst_code_t {
	struct inst_operation_t *operation;
	int type;
};

enum {
	INST_CODE, 
	INST_CODE_RET};


inst_code_t *inst_code_init(inst_operation_t *operation) {
	inst_code_t *instruction = malloc(sizeof(inst_code_t));

	instruction->type = INST_CODE;
	instruction->operation = operation;

	return instruction;
}

inst_code_t *inst_code_ret_init(inst_operation_t *operation) {
	inst_code_t *instruction = malloc(sizeof(inst_code_t));

	instruction->type = INST_CODE_RET;
	instruction->operation = operation;

	return instruction;
}

char *inst_code_get_str_type(int type) {
	switch(type) {
		case INST_CODE:
			return "inst_code";
		case INST_CODE_RET:
			return "inst_code_return";
	}

	return "undefined";
}

char *inst_code_get_json_tree(inst_code_t *instruction) {
	char *res = NULL;
	char *instructionType = inst_operation_get_str_type(instruction->type);
	
	char *jsonOperation = inst_operation_get_json_tree(instruction->operation);
	res = util_asprintf("{\"type\":\"%s\", \"operation\":%s}", instructionType, jsonOperation);
	free(jsonOperation);

	return res;
}

memo_valeur_t *inst_code_exec(inst_code_t *instruction, memo_vars_t *memory, fonction_list_t *fonctions) {
	memo_valeur_t *res = NULL;

	res = inst_operation_exec(instruction->operation, memory, fonctions);

	if(instruction->type == INST_CODE) {
		return NULL;
	} else if(instruction->type == INST_CODE_RET) {
		return res;
	}

	return NULL;
}

void inst_code_free(inst_code_t *instruction) {
	inst_operation_free(instruction->operation);
	free(instruction);
}