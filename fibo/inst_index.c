
struct inst_index_t {
	struct inst_operation_t *operation;
	struct inst_index_t *before;
};

inst_index_t *inst_index_init(inst_operation_t *operation, inst_index_t *before) {
	inst_index_t *instruction = malloc(sizeof(inst_index_t));

	instruction->operation = operation;
	instruction->before = before;

	return instruction;
}

char *inst_index_get_json_tree(inst_index_t *instruction) {
	char *res = NULL;

	if(instruction->before == NULL) {
		char *jsonOperation = inst_operation_get_json_tree(instruction->operation);
		res = util_asprintf("{\"operation\":%s}", jsonOperation);
		free(jsonOperation);
	} else {
		char *jsonOperation = inst_operation_get_json_tree(instruction->operation);
		char *jsonIndex = inst_index_get_json_tree(instruction->before);
		res = util_asprintf("{\"operation\":%s, \"before\":%s}", jsonOperation, jsonIndex);
		free(jsonOperation);
		free(jsonIndex);
	}

	return res;
} 

memo_valeur_t *inst_index_set_ptr_exec(
	inst_index_t *instruction, memo_vars_t *memory, fonction_list_t *fonctions, 
	memo_valeur_t *dest_ptr, memo_valeur_t *valeur, int count) {
	
	memo_valeur_t *ptr = dest_ptr;

	if(instruction->before != NULL) {
		ptr = inst_index_set_ptr_exec(instruction->before, memory, fonctions, dest_ptr, valeur, count + 1);
	}

	memo_valeur_t *valeurIndex = inst_operation_exec(instruction->operation, memory, fonctions);

	if(memo_valeur_is_int(valeurIndex)) {
		int index = memo_valeur_get_int(valeurIndex);

		if(count == 0) {
			memo_valeur_list_set(ptr, index, valeur);
		} else {
			return memo_valeur_list_get(ptr, index);
		}
	} else {
		fprintf(stderr, "erreur : les index doivent être des nombres entier\n");
	}

	return NULL;
}

memo_valeur_t *inst_index_get_ptr_exec(inst_index_t *instruction, memo_vars_t *memory, fonction_list_t *fonctions, memo_valeur_t *dest_ptr) {
	memo_valeur_t *ptr = dest_ptr;

	if(instruction->before != NULL) {
		ptr = inst_index_get_ptr_exec(instruction->before, memory, fonctions, dest_ptr);
	}

	memo_valeur_t *valeurIndex = inst_operation_exec(instruction->operation, memory, fonctions);

	if(memo_valeur_is_int(valeurIndex)) {
		int index = memo_valeur_get_int(valeurIndex);

		return memo_valeur_list_get(ptr, index);
	} else {
		fprintf(stderr, "erreur : les index doivent être des nombres entier\n");
	}

	return NULL;
}

void inst_index_free(inst_index_t *instruction) {
	inst_operation_free(instruction->operation);
	if(instruction->before != NULL) {
		inst_index_free(instruction->before);
	}
	free(instruction);
}