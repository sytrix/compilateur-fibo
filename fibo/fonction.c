
struct fonction_config_t {
	char *cmd;
	int nbArg;
	int *listArg;
	memo_valeur_t *(*fonction)(memo_valeur_t*, char*, inst_code_t*, memo_vars_t *memory, fonction_list_t *fonctions);

};

struct fonction_list_t {
	int length;
	struct fonction_config_t **list;
};


fonction_list_t *fonction_init() {
	fonction_list_t *this = malloc(sizeof(fonction_list_t));

	this->length = 0;
	this->list = NULL;

	int uArg[] = {VARTYPE_LIST, VARTYPE_INT};
	int rangeArg[] = {VARTYPE_INT, VARTYPE_INT, VARTYPE_INT};
	int countArg[] = {VARTYPE_LIST};

	fonction_add(this, "u", 2, uArg, &fonction_def_u);
	fonction_add(this, "range", 3, rangeArg, &fonction_def_range);
	fonction_add(this, "count", 1, countArg, &fonction_def_length);

	return this;
}

void fonction_add(fonction_list_t *this, char *cmd, int nbArg, int *listArg, memo_valeur_t *(*fonction)(memo_valeur_t*, char*, inst_code_t*, memo_vars_t *memory, fonction_list_t *fonctions)) {

	fonction_config_t *config = malloc(sizeof(fonction_config_t));
	config->cmd = cmd;
	config->nbArg = nbArg;
	config->listArg = listArg;
	config->fonction = fonction;

	if(this->length == 0) {
		this->list = malloc(sizeof(fonction_config_t*));
	} else {
		this->list = realloc(this->list, sizeof(fonction_config_t*) * (this->length + 1));
	}

	this->list[this->length] = config;
	this->length++;
}

void fonction_free(fonction_list_t *this) {
	if(this->list != NULL) {
		int i;

		for(i = 0; i < this->length; i++) {
			fonction_config_t *config = this->list[i];
			free(config);
		}

		free(this->list);
	}

	free(this);
}

memo_valeur_t *fonction_execute(fonction_list_t *this, char *cmd, memo_valeur_t *arguments, char *parametre, inst_code_t *instruction, memo_vars_t *memory) {
	memo_valeur_t *res = NULL;
	int i, j;
	int find = 0;
	int erreurType = 0;
	for(i = 0; i < this->length; i++) {
		fonction_config_t *config = this->list[i];

		if(strcmp(config->cmd, cmd) == 0) {
			int nbArg = memo_valeur_list_len(arguments);
			find = 1;

			if(config->nbArg == nbArg) {

				for(j = 0; j < nbArg; j++) {
					if(config->listArg[j] == VARTYPE_INT) {
						if(!memo_valeur_is_int(memo_valeur_list_get(arguments, j))) {
							fprintf(stderr, "erreur : argument %d de la fontion \"%s\" doit être de type int\n", j, cmd);
							erreurType = 1;
						}
					}
					if(config->listArg[j] == VARTYPE_LIST) {
						if(!memo_valeur_is_list(memo_valeur_list_get(arguments, j))) {
							fprintf(stderr, "erreur : argument %d de la fontion \"%s\" doit être de type list\n", j, cmd);
							erreurType = 1;
						}
					}
					if(config->listArg[j] == VARTYPE_STR) {
						if(!memo_valeur_is_str(memo_valeur_list_get(arguments, j))) {
							fprintf(stderr, "erreur : argument %d de la fontion \"%s\" doit être de type string\n", j, cmd);
							erreurType = 1;
						}
					}
				}

				if(!erreurType) {
					res = config->fonction(arguments, parametre, instruction, memory, this);
				}
			} else {
				fprintf(stderr, "erreur : la fonction \"%s\" doit posséder %d paramètre(s) et non %d paramètre(s)\n", cmd, config->nbArg, nbArg);
			}
		}
	}

	if(find == 0) {
		fprintf(stderr, "erreur : la fonction \"%s\" n'est pas définie\n", cmd);
	}

	memo_valeur_free(arguments);
	return res;
}


memo_valeur_t *fonction_def_u(memo_valeur_t *arguments, char *parametre, inst_code_t *code, memo_vars_t *memory, fonction_list_t *fonctions) {
	memo_valeur_t *valeurList = memo_valeur_dup(memo_valeur_list_get(arguments, 0));
	int nbIteration = memo_valeur_get_int(memo_valeur_list_get(arguments, 1));
	int i;
	for(i = 0; i < nbIteration; i++) {
		memo_vars_put(memory, parametre, valeurList);
		memo_valeur_t *valeurRetour = inst_code_exec(code, memory, fonctions);
		if(valeurRetour == NULL) {
			fprintf(stderr, "valeur de retour null à l'index : %d\n", i);
			return NULL;
		} else {
			memo_valeur_list_add(valeurList, valeurRetour);
		}
	}

	return valeurList;
}


memo_valeur_t *fonction_def_range(memo_valeur_t *arguments, char *parametre, inst_code_t *code, memo_vars_t *memory, fonction_list_t *fonctions) {
	int rangeDeb = memo_valeur_get_int(memo_valeur_list_get(arguments, 0));
	int rangeFin = memo_valeur_get_int(memo_valeur_list_get(arguments, 1));
	int rangePas = memo_valeur_get_int(memo_valeur_list_get(arguments, 2));
	
	int nbIteration = (rangeFin - 1 - rangeDeb) / rangePas + 1;
	int currentIndex = rangeDeb;

	memo_valeur_t *valeurList = memo_valeur_list_init(nbIteration);
	int i;
	for(i = 0; i < nbIteration; i++) {
		memo_vars_put(memory, parametre, memo_valeur_int_init(currentIndex));
		memo_valeur_t *valeurRetour = inst_code_exec(code, memory, fonctions);
		if(valeurRetour == NULL) {
			fprintf(stderr, "valeur de retour null à l'index : %d\n", i);
			return NULL;
		} else {
			memo_valeur_list_set(valeurList, i, valeurRetour);
			currentIndex += rangePas;
		}
	}

	return valeurList;
}

memo_valeur_t *fonction_def_length(memo_valeur_t *arguments, char *parametre, inst_code_t *code, memo_vars_t *memory, fonction_list_t *fonctions) {
	int length = memo_valeur_list_len(memo_valeur_list_get(arguments, 0));

	return memo_valeur_int_init(length);
}
