/*

===========================================================================================================
===											DESCRIPTION													===
===========================================================================================================

Ce fichier contient toutes les déclarations de structure et fonctions
On peut trouver 3 catégories de structures et fonctions reconnaissablent selon leurs prefix

fonction_ :		Fonction correspond à la blibliothéque des fonctions que l'on peut retrouver dans le langage fibo
inst_ :			Instruction correspond à chaque petit morceau d'operation que l'on peut retrouver dans le fichier bison
memo_ :			Memoire correspond à ce qui est stoquer dans chaque variable (c'est un dictionnaire qui associe nom de variable avec sa valeur)

*/


// util défini les fonctions qui pourront être utilisé communément comme outils dans le projet
#include "util.h"
#include "util.c"

// les différents types de variable défini dans le langage
enum {
	VARTYPE_INT, 
	VARTYPE_LIST, 
	VARTYPE_STR};

/*

===========================================================================================================
===											STRUCTURES													===
===========================================================================================================

*/

// chaque fonction possède structure "configuration" (nb param, types des params) et une structure list qui retient l'ensemble des fonctions configurés
typedef struct fonction_config_t 			fonction_config_t;
typedef struct fonction_list_t 				fonction_list_t;

// chaque morceau d'instruction defini dans la grammaire est enregistrer sous la forme d'une structure
// selon la structure on peut facilement retrouver des pointeurs vers d'autre structure qui sont les instructions 
// qui vont se succéder lors de l'execution
typedef struct inst_operation_t 			inst_operation_t;
typedef struct inst_operation_math_t 		inst_operation_math_t;
typedef struct inst_operation_valeur_t 		inst_operation_valeur_t;
typedef struct inst_operation_affectation_t inst_operation_affectation_t;
typedef struct inst_tableau_t 				inst_tableau_t;
typedef struct inst_valeur_t 				inst_valeur_t;
typedef struct inst_valeur_fonctiondesc_t 	inst_valeur_fonctiondesc_t;
typedef struct inst_valeur_fonction_t 		inst_valeur_fonction_t;
typedef struct inst_valeur_variable_t 		inst_valeur_variable_t;
typedef struct inst_valeur_chiffre_t 		inst_valeur_chiffre_t;
typedef struct inst_valeur_chaine_t 		inst_valeur_chaine_t;
typedef struct inst_valeur_tableau_t		inst_valeur_tableau_t;
typedef struct inst_valeur_valindex_t		inst_valeur_valindex_t;
typedef struct inst_index_t 				inst_index_t;
typedef struct inst_variable_t 				inst_variable_t;
typedef struct inst_arguments_t 			inst_arguments_t;
typedef struct inst_code_t 					inst_code_t;

// memo_valeur_ : 	chaque valeur retenu en mémoire possède une structure selon sont type
// memo_vars_ :		une liste chainé qui fait correspondre un nom de variable à une structure de valeur
typedef struct memo_valeur_t 				memo_valeur_t;
typedef struct memo_valeur_int_t 			memo_valeur_int_t;
typedef struct memo_valeur_list_t 			memo_valeur_list_t;
typedef struct memo_valeur_str_t 			memo_valeur_str_t;
typedef struct memo_vars_node_t 			memo_vars_node_t;
typedef struct memo_vars_t 					memo_vars_t;


/*
===========================================================================================================
===											FONCTIONS MEMOIRE											===
===========================================================================================================
*/


// les fonctions ci dessous permettent d'initialiser les valeurs de chaque type
memo_valeur_t *		memo_valeur_int_init(long long valeur);
memo_valeur_t *		memo_valeur_list_init(int length);
memo_valeur_t *		memo_valeur_str_init(char *chaine);

memo_valeur_t * 	memo_valeur_dup(memo_valeur_t *todup); // dup permet de faire une copie en mémoire d'une valeur qu'on lui fournie en parametre
memo_valeur_t *		memo_valeur_compute(memo_valeur_t *a, memo_valeur_t *b, char sign); // compute permet de faire des operations avec des signes (+-*/) entre deux valeurs
// les fonctions ci dessous permettent d'effectuer des operations sur les tableaux/listes 
int 				memo_valeur_list_len(memo_valeur_t *this);
void 				memo_valeur_list_add(memo_valeur_t *this, memo_valeur_t *valeur);
void 				memo_valeur_list_set(memo_valeur_t *this, int index, memo_valeur_t *valeur);
memo_valeur_t *		memo_valeur_list_get(memo_valeur_t *this, int index);
// les fonctions ci dessous permettent de verifier le type d'une valeur fournie en param
int 				memo_valeur_is_int(memo_valeur_t *this);
int 				memo_valeur_is_list(memo_valeur_t *this);
int 				memo_valeur_is_str(memo_valeur_t *this);

int 				memo_valeur_get_int(memo_valeur_t *this); // sert à obtenir l'entier contenu dans la valeur (cette fonction sert lors de la récupération d'un index)
long long 			memo_valeur_get_long_int(memo_valeur_t *this); // sert à obtenir la valeur exact de l'entier contenu dans la valeur
char * 				memo_valeur_get_str(memo_valeur_t *this); // sert à obtenir la valeur de la chaine de char
char *				memo_valeur_get_str_type(int type); // sert à avoir le nom littéral du type de la valeur (exemple int -> "int")
char *				memo_valeur_get_json_tree(memo_valeur_t *this); // sert à afficher la valeur de manière détailler en format JSON
char *				memo_valeur_get_value_tree(memo_valeur_t *this); // sert à afficher la valeur de manière simpliste
void 				memo_valeur_free(memo_valeur_t *this); // liberation mémoire de la valeur


// memo_vars est une liste chainé de variable (dictionnaire)
memo_vars_t *		memo_vars_init();
void 				memo_vars_put(memo_vars_t *this, char *nom, memo_valeur_t *valeur);
memo_valeur_t *		memo_vars_get(memo_vars_t *this, char *nom);
memo_valeur_t *		memo_vars_get_ptr(memo_vars_t *this, char *nom);
char *				memo_vars_get_json_tree(memo_vars_t *this); // obtention de la liste des variables contenu en mémoire avec leurs valeurs correspondantes en format JSON
void 				memo_vars_free(memo_vars_t *this);

/*
===========================================================================================================
===											FONCTIONS INSTRUCTION										===
===========================================================================================================

Les fonctions suivantes permettent d'enregistrer chaque morceau de la grammaire
On retrouvent régulièrement des fonctions comportant les suffix suivants:
- init 				pour initialiser chaque structures pendant le parsing
- free 				pour liberer la mémoire une fois l'execution effectuer
- exec 				permettant d'implementer tout le comportement du programme lors de l'execution
- get_json_tree 	permettant d'afficher un arbre json de toutes les structures qui sont parcourue lors de l'execution
						(cette fonction sert pour le debug uniquement)
- get_str_type 		permet de renvoyé le type d'une structure de manière littérale (exemple INST_OPERATION_MATH -> "inst_operation_math")

*/

inst_arguments_t *	inst_arguments_init(inst_operation_t *operation, inst_arguments_t *before);
char *				inst_arguments_get_json_tree(inst_arguments_t *instruction);
memo_valeur_t *		inst_arguments_exec(inst_arguments_t *instruction, memo_vars_t *memory, fonction_list_t *fonctions, int count);
void 				inst_arguments_free(inst_arguments_t *instruction);


inst_code_t *		inst_code_init(inst_operation_t *operation);
inst_code_t *		inst_code_ret_init(inst_operation_t *operation);
char *				inst_code_get_str_type(int type);
char *				inst_code_get_json_tree(inst_code_t *instruction);
memo_valeur_t *		inst_code_exec(inst_code_t *instruction, memo_vars_t *memory, fonction_list_t *fonctions);
void 				inst_code_free(inst_code_t *instruction);


inst_index_t *		inst_index_init(inst_operation_t *operation, inst_index_t *before);
char *				inst_index_get_json_tree(inst_index_t *instruction);
memo_valeur_t *		inst_index_set_ptr_exec(inst_index_t *instruction, memo_vars_t *memory, fonction_list_t *fonctions, memo_valeur_t *dest_ptr, memo_valeur_t *valeur, int count);
memo_valeur_t *		inst_index_get_ptr_exec(inst_index_t *instruction, memo_vars_t *memory, fonction_list_t *fonctions, memo_valeur_t *dest_ptr);
void 				inst_index_free(inst_index_t *instruction);


inst_operation_t *	inst_operation_math_init(inst_operation_t *previous, inst_valeur_t *valeur, char sign);
inst_operation_t *	inst_operation_valeur_init(inst_valeur_t *valeur);
inst_operation_t *	inst_operation_affectation_init(inst_operation_t *operation, inst_variable_t *variable);
char *				inst_operation_get_str_type(int type);
char *				inst_operation_get_json_tree(inst_operation_t *instruction);
memo_valeur_t *		inst_operation_exec(inst_operation_t *instruction, memo_vars_t *memory, fonction_list_t *fonctions);
void 				inst_operation_free(inst_operation_t *instruction);


inst_tableau_t *	inst_tableau_init(inst_tableau_t *before, inst_operation_t *operation);
char *				inst_tableau_get_json_tree(inst_tableau_t *instruction);
memo_valeur_t *		inst_tableau_exec(inst_tableau_t *instruction, memo_vars_t *memory, fonction_list_t *fonctions, int count);
void 				inst_tableau_free(inst_tableau_t *instruction);


inst_valeur_t *		inst_valeur_fonctiondesc_init(
						char *nomFonction, inst_arguments_t *arguments, 
						char *nomFonctionOutput, inst_code_t *code);
inst_valeur_t *		inst_valeur_fonction_init(char *nomFonction, inst_arguments_t *arguments);
inst_valeur_t *		inst_valeur_variable_init(char *variable);
inst_valeur_t *		inst_valeur_chiffre_init(int chiffre);
inst_valeur_t *		inst_valeur_chaine_init(char *chaine);
inst_valeur_t *		inst_valeur_tableau_init(inst_tableau_t *tableau);
inst_valeur_t *		inst_valeur_valindex_init(inst_valeur_t *valeur, inst_index_t *index);
char *				inst_valeur_get_str_type(int type);
char *				inst_valeur_get_json_tree(inst_valeur_t *instruction);
memo_valeur_t *		inst_valeur_exec(inst_valeur_t *instruction, memo_vars_t *memory, fonction_list_t *fonctions);
void 				inst_valeur_free(inst_valeur_t *instruction);


inst_variable_t *	inst_variable_init(inst_index_t *index, char *nom);
char *				inst_variable_get_json_tree(inst_variable_t *instruction);
void				inst_variable_affectation_exec(inst_variable_t *instruction, memo_vars_t *memory, fonction_list_t *fonctions, memo_valeur_t *valeur);
void 				inst_variable_free(inst_variable_t *instruction);

/*
===========================================================================================================
===											FONCTIONS DE FONCTIONS										===
===========================================================================================================
*/

// les fonctions suivantes permettent d'enregistrer une fonctions avec sa config dans une liste de fonctions
fonction_list_t *	fonction_init();
void 				fonction_add(fonction_list_t *fonctions, char *cmd, int nbArg, int *listArg, memo_valeur_t *(*fonction)(memo_valeur_t*, char*, inst_code_t*, memo_vars_t *memory, fonction_list_t *fonctions));
void 				fonction_free(fonction_list_t *fonctions);
memo_valeur_t *		fonction_execute(fonction_list_t *fonctions, char *cmd, memo_valeur_t *arguments, char *parametre, inst_code_t *instruction, memo_vars_t *memory);

// ici on retrouve les fonctions utilise dans le programme qui sont directement implementer en C
memo_valeur_t *		fonction_def_u(memo_valeur_t *arguments, char *parametre, inst_code_t *code, memo_vars_t *memory, fonction_list_t *fonctions);
memo_valeur_t *		fonction_def_range(memo_valeur_t *arguments, char *parametre, inst_code_t *code, memo_vars_t *memory, fonction_list_t *fonctions);
memo_valeur_t *		fonction_def_length(memo_valeur_t *arguments, char *parametre, inst_code_t *code, memo_vars_t *memory, fonction_list_t *fonctions);

/*
===========================================================================================================
===										INCLUDE DES IMPLEMENTATIONS EN C								===
===========================================================================================================
*/

#include "fonction.c"
#include "memo_valeur.c"
#include "memo_vars.c"
#include "inst_operation.c"
#include "inst_tableau.c"
#include "inst_valeur.c"
#include "inst_variable.c"
#include "inst_index.c"
#include "inst_arguments.c"
#include "inst_code.c"