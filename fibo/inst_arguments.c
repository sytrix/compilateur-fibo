
struct inst_arguments_t {
	struct inst_operation_t *operation;
	struct inst_arguments_t *before;
};

inst_arguments_t *inst_arguments_init(inst_operation_t *operation, inst_arguments_t *before) {
	inst_arguments_t *instruction = malloc(sizeof(inst_arguments_t));

	instruction->operation = operation;
	instruction->before = before;

	return instruction;
}

char *inst_arguments_get_json_tree(inst_arguments_t *instruction) {
	char *res = NULL;

	if(instruction->before == NULL) {
		char *jsonOperation = inst_operation_get_json_tree(instruction->operation);
		res = util_asprintf("{\"operation\":%s}", jsonOperation);
		free(jsonOperation);
	} else {
		char *jsonOperation = inst_operation_get_json_tree(instruction->operation);
		char *jsonArguments = inst_arguments_get_json_tree(instruction->before);
		res = util_asprintf("{\"operation\":%s, \"before\":%s}", jsonOperation, jsonArguments);
		free(jsonOperation);
		free(jsonArguments);
	}

	return res;
}

memo_valeur_t *inst_arguments_exec(inst_arguments_t *instruction, memo_vars_t *memory, fonction_list_t *fonctions, int count) {
	memo_valeur_t *res;

	if(instruction->before != NULL) {
		res = inst_arguments_exec(instruction->before, memory, fonctions, count + 1);
		if(res == NULL) {
			return NULL;
		}
	} else {
		res = memo_valeur_list_init(count + 1);
	}

	memo_valeur_t *valeur = inst_operation_exec(instruction->operation, memory, fonctions);
	
	if(valeur == NULL) {
		return NULL;
	}

	memo_valeur_list_set(res, count, valeur);

	if(count == 0) {
		memo_valeur_list_reverse(res);
	}

	return res;


}

void inst_arguments_free(inst_arguments_t *instruction) {
	inst_operation_free(instruction->operation);

	if(instruction->before != NULL) {
		inst_arguments_free(instruction->before);
	}

	free(instruction);
}