
struct memo_vars_node_t {
	struct memo_vars_node_t *suiv;
	struct memo_valeur_t *valeur;
	char *nom;
};

struct memo_vars_t {
	struct memo_vars_node_t *head;
	int count;
};

memo_vars_t *memo_vars_init() {
	memo_vars_t *this = malloc(sizeof(memo_vars_t)); 

	this->head = NULL;
	this->count = 0;

	return this;
}

void memo_vars_put(memo_vars_t *this, char *nom, memo_valeur_t *valeur) {
	memo_vars_node_t *current = this->head;
	memo_vars_node_t *last = NULL;

	while(current != NULL) {
		if(strcmp(current->nom, nom) == 0) {
			memo_valeur_free(current->valeur);
			current->valeur = memo_valeur_dup(valeur);
			return;
		}

		if(current->suiv == NULL) {
			last = current;
			current = NULL;
		} else {
			current = current->suiv;
		}
	}
	
	memo_vars_node_t *newNode = malloc(sizeof(memo_vars_node_t));

	newNode->suiv = NULL;
	newNode->valeur = memo_valeur_dup(valeur);
	newNode->nom = strdup(nom);

	if(this->head == NULL) {
		this->head = newNode;
	} else {
		last->suiv = newNode;
	}

	this->count++;
}

memo_valeur_t *memo_vars_get(memo_vars_t *this, char *nom) {
	memo_valeur_t *valeur = memo_vars_get_ptr(this, nom);

	if(valeur == NULL) {
		return NULL;
	}

	return memo_valeur_dup(valeur);
}

memo_valeur_t *memo_vars_get_ptr(memo_vars_t *this, char *nom) {
	memo_vars_node_t *current = this->head;

	while(current != NULL) {
		if(strcmp(current->nom, nom) == 0) {
			return current->valeur;
		} 

		current = current->suiv;
	}

	fprintf(stderr, "erreur : \"%s\" n'existe pas\n", nom);

	return NULL;
}

char *memo_vars_get_json_tree(memo_vars_t *this) {
	int count = this->count;
	char *res = NULL;
	char **listJson = malloc(sizeof(char*) * count);
	char **listNom = malloc(sizeof(char*) * count);
	int needed = 0;
	int i;
	
	memo_vars_node_t *current = this->head;

	for(i = 0; i < count; i++) {
		listJson[i] = memo_valeur_get_json_tree(current->valeur);
		listNom[i] = current->nom;
		needed += strlen(listJson[i]) + 2 + strlen(listNom[i]) + 21;
		current = current->suiv;
	}

	res = malloc(sizeof(char) * (needed + 3));

	strcpy(res, "[");

	for(i = 0; i < count; i++) {
		strcat(res, "{\"nom\":\"");
		strcat(res, listNom[i]);
		strcat(res, "\", \"valeur\":");
		strcat(res, listJson[i]);
		strcat(res, "}");
		if(i < count - 1) {
			strcat(res, ", ");
		}
		free(listJson[i]);
	}

	strcat(res, "]");

	free(listJson);
	free(listNom);
	
	return res;
}

void memo_vars_node_free(memo_vars_node_t *node) {
	if(node->suiv != NULL) {
		memo_vars_node_free(node->suiv);
	}

	memo_valeur_free(node->valeur);
	free(node->nom);
	free(node);
}

void memo_vars_free(memo_vars_t *this) {
	if(this->head != NULL) {
		memo_vars_node_free(this->head);
	}
}