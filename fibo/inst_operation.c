#include <stdio.h>

struct inst_operation_t {
	int type;
};

struct inst_operation_math_t {
	int type;
	struct inst_operation_t *previous;
	struct inst_valeur_t *valeur;
	char sign;
};

struct inst_operation_valeur_t {
	int type;
	struct inst_valeur_t *valeur;
};

struct inst_operation_affectation_t {
	int type;
	struct inst_operation_t *operation;
	struct inst_variable_t *variable;
};

enum {
	INST_OPERATION_MATH, 
	INST_OPERATION_VALEUR,
	INST_OPERATION_AFFECTATION};

inst_operation_t *inst_operation_math_init(inst_operation_t *previous, inst_valeur_t *valeur, char sign) {
	inst_operation_math_t *instruction = malloc(sizeof(inst_operation_math_t));

	instruction->type = INST_OPERATION_MATH;
	instruction->previous = previous;
	instruction->valeur = valeur;
	instruction->sign = sign;

	return (inst_operation_t*)instruction;
}

inst_operation_t *inst_operation_valeur_init(inst_valeur_t *valeur) {
	inst_operation_valeur_t *instruction = malloc(sizeof(inst_operation_valeur_t));

	instruction->type = INST_OPERATION_VALEUR;
	instruction->valeur = valeur;

	return (inst_operation_t*)instruction;
}

inst_operation_t *inst_operation_affectation_init(inst_operation_t *operation, inst_variable_t *variable) {
	inst_operation_affectation_t *instruction = malloc(sizeof(inst_operation_affectation_t));

	instruction->type = INST_OPERATION_AFFECTATION;
	instruction->operation = operation;
	instruction->variable = variable;

	return (inst_operation_t*)instruction;
}

char *inst_operation_get_str_type(int type) {
	switch(type) {
		case INST_OPERATION_MATH:
			return "inst_operation_math";
		case INST_OPERATION_VALEUR:
			return "inst_operation_valeur";
		case INST_OPERATION_AFFECTATION:
			return "inst_operation_affectation";
	}

	return "undefined";
}

char *inst_operation_get_json_tree(inst_operation_t *instruction) {
	char *res = NULL;
	char *instructionType = inst_operation_get_str_type(instruction->type);

	if(instruction->type == INST_OPERATION_MATH) {
		inst_operation_math_t *instOperationMath = (inst_operation_math_t*)instruction;
		char *jsonPrevious = inst_operation_get_json_tree(instOperationMath->previous);
		char *jsonValeur = inst_valeur_get_json_tree(instOperationMath->valeur);
		char sign = instOperationMath->sign;
		res = util_asprintf("{\"type\":\"%s\", \"precedent\":%s, \"valeur\":%s, \"sign\":\"%c\"}", instructionType, jsonPrevious, jsonValeur, sign);
		free(jsonPrevious);
		free(jsonValeur);
	} else if(instruction->type == INST_OPERATION_VALEUR) {
		inst_operation_valeur_t *instOperationValeur = (inst_operation_valeur_t*)instruction;
		char *jsonValeur = inst_valeur_get_json_tree(instOperationValeur->valeur);
		res = util_asprintf("{\"type\":\"%s\", \"valeur\":%s}", instructionType, jsonValeur);
		free(jsonValeur);
	} else if(instruction->type == INST_OPERATION_AFFECTATION) {
		inst_operation_affectation_t *instOperationAffectation = (inst_operation_affectation_t*)instruction;
		char *jsonOperation = inst_operation_get_json_tree(instOperationAffectation->operation);
		char *jsonVariable = inst_variable_get_json_tree(instOperationAffectation->variable);
		res = util_asprintf("{\"type\":\"%s\", \"operation\":%s, \"variable\":%s}", instructionType, jsonOperation, jsonVariable);
		free(jsonOperation);
		free(jsonVariable);
	}

	return res;
}

memo_valeur_t *inst_operation_exec(inst_operation_t *instruction, memo_vars_t *memory, fonction_list_t *fonctions) {
	memo_valeur_t *res = NULL;

	if(instruction->type == INST_OPERATION_MATH) {
		inst_operation_math_t *instOperationMath = (inst_operation_math_t*)instruction;
		memo_valeur_t *a = inst_operation_exec(instOperationMath->previous, memory, fonctions);
		memo_valeur_t *b = inst_valeur_exec(instOperationMath->valeur, memory, fonctions);
		char sign = instOperationMath->sign;
		
		if(a != NULL && b != NULL) {
			res = memo_valeur_compute(a, b, sign);
		}
		
		if(a != NULL) {
			memo_valeur_free(a);
		}

		if(b != NULL) {
			memo_valeur_free(b);
		}
		
	} else if(instruction->type == INST_OPERATION_VALEUR) {
		inst_operation_valeur_t *instOperationValeur = (inst_operation_valeur_t*)instruction;
		res = inst_valeur_exec(instOperationValeur->valeur, memory, fonctions);
	} else if(instruction->type == INST_OPERATION_AFFECTATION) {
		inst_operation_affectation_t *instOperationAffectation = (inst_operation_affectation_t*)instruction;
		memo_valeur_t *valeur = inst_operation_exec(instOperationAffectation->operation, memory, fonctions);
		if(valeur != NULL) {
			inst_variable_affectation_exec(instOperationAffectation->variable, memory, fonctions, valeur);
			res = valeur;
		}

	}

	return res;
}

void inst_operation_free(inst_operation_t *instruction) {
	
	if(instruction->type == INST_OPERATION_MATH) {
		inst_operation_math_t *instOperationMath = (inst_operation_math_t*)instruction;
		inst_operation_free(instOperationMath->previous);
		inst_valeur_free(instOperationMath->valeur);
		free(instOperationMath);
	} else if(instruction->type == INST_OPERATION_VALEUR) {
		inst_operation_valeur_t *instOperationValeur = (inst_operation_valeur_t*)instruction;
		inst_valeur_free(instOperationValeur->valeur);
		free(instOperationValeur);
	} else if(instruction->type == INST_OPERATION_AFFECTATION) {
		inst_operation_affectation_t *instOperationAffectation = (inst_operation_affectation_t*)instruction;
		inst_operation_free(instOperationAffectation->operation);
		inst_variable_free(instOperationAffectation->variable);
		free(instOperationAffectation);
	}
}