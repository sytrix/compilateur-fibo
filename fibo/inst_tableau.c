

struct inst_tableau_t
{
	struct inst_tableau_t *before;
	struct inst_operation_t *operation;
};


inst_tableau_t *inst_tableau_init(inst_tableau_t *before, inst_operation_t *operation) {
	inst_tableau_t *this = malloc(sizeof(inst_tableau_t));

	this->before = before;
	this->operation = operation;

	return this;
}

char *inst_tableau_get_json_tree(inst_tableau_t *instruction) {
	char *res = NULL;

	if(instruction->before == NULL) {
		char *jsonOperation = inst_operation_get_json_tree(instruction->operation);
		res = util_asprintf("{\"operation\":%s}", jsonOperation);
		free(jsonOperation);
	} else {
		char *jsonOperation = inst_operation_get_json_tree(instruction->operation);
		char *jsonTableau = inst_tableau_get_json_tree(instruction->before);
		res = util_asprintf("{\"operation\":%s, \"before\":%s}", jsonOperation, jsonTableau);
		free(jsonOperation);
		free(jsonTableau);
	}

	return res;
}

memo_valeur_t *inst_tableau_exec(inst_tableau_t *instruction, memo_vars_t *memory, fonction_list_t *fonctions, int count) {
	memo_valeur_t *res = NULL;

	if(instruction->before != NULL) {
		res = inst_tableau_exec(instruction->before, memory, fonctions, count + 1);

		if(res == NULL) {
			return NULL;
		}

	} else {
		res = memo_valeur_list_init(count + 1);
	}

	memo_valeur_t *valeur = inst_operation_exec(instruction->operation, memory, fonctions);

	if(valeur == NULL) {
		return NULL;
	}


	memo_valeur_list_set(res, count, valeur);

	if(count == 0) {
		memo_valeur_list_reverse(res);
	}

	return res;
}

void inst_tableau_free(inst_tableau_t *instruction) {
	if(instruction->before != NULL) {
		inst_tableau_free(instruction->before);
	}
	inst_operation_free(instruction->operation);
	free(instruction);

}

