#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

char *util_asprintf(char *format, ...) {
	va_list args; 
	va_start(args, format);
    size_t needed = vsnprintf(NULL, 0, format, args) + 1;
    va_end(args);
    char  *buffer = malloc(needed);
    va_start(args, format);
    va_end(args);
    vsnprintf(buffer, needed, format, args);
    
    return buffer;

}