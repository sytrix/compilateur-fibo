

struct inst_valeur_t {
	int type;
};

struct inst_valeur_fonctiondesc_t {
	int type;
	char *nomFonction;
	struct inst_arguments_t *arguments;
	char *nomFonctionOutput;
	struct inst_code_t *code;
};

struct inst_valeur_fonction_t {
	int type;
	char *nomFonction;
	struct inst_arguments_t *arguments;
};

struct inst_valeur_variable_t {
	int type;
	char *variable;
};

struct inst_valeur_chiffre_t {
	int type;
	int chiffre;
};

struct inst_valeur_chaine_t {
	int type;
	char *chaine;
};

struct inst_valeur_tableau_t {
	int type;
	struct inst_tableau_t *tableau;
};

struct inst_valeur_valindex_t {
	int type;
	struct inst_valeur_t *valeur;
	struct inst_index_t *index;
};

enum {
	INST_VALEUR_FONCTIONDESC, 
	INST_VALEUR_FONCTION,
	INST_VALEUR_VARIABLE,
	INST_VALEUR_CHIFFRE,
	INST_VALEUR_CHAINE,
	INST_VALEUR_TABLEAU,
	INST_VALEUR_VALINDEX};

inst_valeur_t *inst_valeur_fonctiondesc_init(
	char *nomFonction, inst_arguments_t *arguments, 
	char *nomFonctionOutput, inst_code_t *code)
{
	inst_valeur_fonctiondesc_t *instruction = malloc(sizeof(inst_valeur_fonctiondesc_t));

	instruction->type = INST_VALEUR_FONCTIONDESC;
	instruction->nomFonction = nomFonction;
	instruction->arguments = arguments;
	instruction->nomFonctionOutput = nomFonctionOutput;
	instruction->code = code;

	return (inst_valeur_t*)instruction;
}

inst_valeur_t *inst_valeur_fonction_init(char *nomFonction, inst_arguments_t *arguments)
{
	inst_valeur_fonction_t *instruction = malloc(sizeof(inst_valeur_fonction_t));

	instruction->type = INST_VALEUR_FONCTION;
	instruction->nomFonction = nomFonction;
	instruction->arguments = arguments;

	return (inst_valeur_t*)instruction;
}

inst_valeur_t *inst_valeur_variable_init(char *variable)
{
	inst_valeur_variable_t *instruction = malloc(sizeof(inst_valeur_variable_t));

	instruction->type = INST_VALEUR_VARIABLE;
	instruction->variable = variable;

	return (inst_valeur_t*)instruction;
}

inst_valeur_t *inst_valeur_chiffre_init(int chiffre)
{
	inst_valeur_chiffre_t *instruction = malloc(sizeof(inst_valeur_chiffre_t));

	instruction->type = INST_VALEUR_CHIFFRE;
	instruction->chiffre = chiffre;

	return (inst_valeur_t*)instruction;
}

inst_valeur_t *inst_valeur_chaine_init(char *chaine)
{
	inst_valeur_chaine_t *instruction = malloc(sizeof(inst_valeur_chaine_t));
	int len = (strlen(chaine) - 2);
	char *cutChaine = malloc(sizeof(char) * (len + 1));
	memcpy(cutChaine, chaine + 1, sizeof(char) * len);

	cutChaine[len] = '\0';

	instruction->type = INST_VALEUR_CHAINE;
	instruction->chaine = cutChaine;

	free(chaine);

	return (inst_valeur_t*)instruction;
}

inst_valeur_t *inst_valeur_tableau_init(inst_tableau_t *tableau)
{
	inst_valeur_tableau_t *instruction = malloc(sizeof(inst_valeur_tableau_t));

	instruction->type = INST_VALEUR_TABLEAU;
	instruction->tableau = tableau;

	return (inst_valeur_t*)instruction;
}

inst_valeur_t *inst_valeur_valindex_init(inst_valeur_t *valeur, inst_index_t *index)
{
	inst_valeur_valindex_t *instruction = malloc(sizeof(inst_valeur_valindex_t));

	instruction->type = INST_VALEUR_VALINDEX;
	instruction->valeur = valeur;
	instruction->index = index;

	return (inst_valeur_t*)instruction;
}

char *inst_valeur_get_str_type(int type) {
	switch(type) {
		case INST_VALEUR_FONCTIONDESC:
			return "inst_valeur_fonction_descriptive";
		case INST_VALEUR_FONCTION:
			return "inst_valeur_fonction";
		case INST_VALEUR_VARIABLE:
			return "inst_valeur_variable";
		case INST_VALEUR_CHIFFRE:
			return "inst_valeur_chiffre";
		case INST_VALEUR_CHAINE:
			return "inst_valeur_chaine";
		case INST_VALEUR_TABLEAU:
			return "inst_valeur_tableau";
		case INST_VALEUR_VALINDEX:
			return "inst_valeur_valindex";
	}

	return "undefined";
}

char *inst_valeur_get_json_tree(inst_valeur_t *instruction) {
	char *res = NULL;
	char *instructionType = inst_valeur_get_str_type(instruction->type);

	if(instruction->type == INST_VALEUR_FONCTIONDESC) {
		inst_valeur_fonctiondesc_t *instFonctionDesc = (inst_valeur_fonctiondesc_t*)instruction;
		char *nomFonction = instFonctionDesc->nomFonction;
		char *jsonArguments = inst_arguments_get_json_tree(instFonctionDesc->arguments);
		char *nomFonctionOutput = instFonctionDesc->nomFonctionOutput;
		char *jsonCode = inst_code_get_json_tree(instFonctionDesc->code);

		res = util_asprintf(
			"{\"type\":\"%s\", \"fonction\":\"%s\", \"arguments\":%s, \"f_output\":\"%s\", \"code\":%s}"
			, instructionType, nomFonction, jsonArguments, nomFonctionOutput, jsonCode);

		free(jsonArguments);
		free(jsonCode);

	} else if(instruction->type == INST_VALEUR_FONCTION) {
		inst_valeur_fonction_t *instFonction = (inst_valeur_fonction_t*)instruction;
		char *nomFonction = instFonction->nomFonction;
		char *jsonArguments = inst_arguments_get_json_tree(instFonction->arguments);

		res = util_asprintf(
			"{\"type\":\"%s\", \"fonction\":\"%s\", \"arguments\":%s}"
			, instructionType, nomFonction, jsonArguments);

		free(jsonArguments);

	} else if(instruction->type == INST_VALEUR_VARIABLE) {
		inst_valeur_variable_t *instVariable = (inst_valeur_variable_t*)instruction;
		char *variable = instVariable->variable;

		res = util_asprintf("{\"type\":\"%s\", \"variable\":\"%s\"}", instructionType, variable);

	} else if(instruction->type == INST_VALEUR_CHIFFRE) {
		inst_valeur_chiffre_t *instChiffre = (inst_valeur_chiffre_t*)instruction;
		int chiffre = instChiffre->chiffre;

		res = util_asprintf("{\"type\":\"%s\", \"chiffre\":%d}", instructionType, chiffre);

	} else if(instruction->type == INST_VALEUR_CHAINE) {
		inst_valeur_chaine_t *instChaine = (inst_valeur_chaine_t*)instruction;
		char *chaine = instChaine->chaine;

		res = util_asprintf("{\"type\":\"%s\", \"chaine\":\"%s\"}", instructionType, chaine);

	} else if(instruction->type == INST_VALEUR_TABLEAU) {
		inst_valeur_tableau_t *instTableau = (inst_valeur_tableau_t*)instruction;
		char *jsonTableau = inst_tableau_get_json_tree(instTableau->tableau);

		res = util_asprintf("{\"type\":\"%s\", \"tableau\":%s}", instructionType, jsonTableau);

		free(jsonTableau);
	} else if(instruction->type == INST_VALEUR_VALINDEX) {
		inst_valeur_valindex_t *instValindex = (inst_valeur_valindex_t*)instruction;
		char *jsonValeur = inst_valeur_get_json_tree(instValindex->valeur);
		char *jsonIndex = inst_index_get_json_tree(instValindex->index);

		res = util_asprintf("{\"type\":\"%s\", \"valeur\":%s, \"index\":%s}", instructionType, jsonValeur, jsonIndex);

		free(jsonValeur);
		free(jsonIndex);
	}

	return res;
}

memo_valeur_t *inst_valeur_exec(inst_valeur_t *instruction, memo_vars_t *memory, fonction_list_t *fonctions) {
	if(instruction->type == INST_VALEUR_FONCTIONDESC) {
		inst_valeur_fonctiondesc_t *instFonctionDesc = (inst_valeur_fonctiondesc_t*)instruction;
		char *nomFonction = instFonctionDesc->nomFonction;
		memo_valeur_t *valeurArguments = inst_arguments_exec(instFonctionDesc->arguments, memory, fonctions, 0);
		char *nomFonctionOutput = instFonctionDesc->nomFonctionOutput;
		inst_code_t *code = instFonctionDesc->code;
		return fonction_execute(fonctions, nomFonction, valeurArguments, nomFonctionOutput, code, memory);
	} else if(instruction->type == INST_VALEUR_FONCTION) {
		inst_valeur_fonction_t *instFonction = (inst_valeur_fonction_t*)instruction;
		char *nomFonction = instFonction->nomFonction;
		memo_valeur_t *valeurArguments = inst_arguments_exec(instFonction->arguments, memory, fonctions, 0);
		return fonction_execute(fonctions, nomFonction, valeurArguments, NULL, NULL, memory);
	} else if(instruction->type == INST_VALEUR_VARIABLE) {
		inst_valeur_variable_t *instVariable = (inst_valeur_variable_t*)instruction;
		memo_valeur_t *valeur = memo_vars_get_ptr(memory, instVariable->variable);
		return memo_valeur_dup(valeur);
	} else if(instruction->type == INST_VALEUR_CHIFFRE) {
		inst_valeur_chiffre_t *instChiffre = (inst_valeur_chiffre_t*)instruction;
		int chiffre = instChiffre->chiffre;
		return memo_valeur_int_init(chiffre);
	} else if(instruction->type == INST_VALEUR_CHAINE) {
		inst_valeur_chaine_t *instChaine = (inst_valeur_chaine_t*)instruction;
		char *chaine = instChaine->chaine;
		return memo_valeur_str_init(chaine);
	} else if(instruction->type == INST_VALEUR_TABLEAU) {
		inst_valeur_tableau_t *instTableau = (inst_valeur_tableau_t*)instruction;
		return inst_tableau_exec(instTableau->tableau, memory, fonctions, 0);
	} else if(instruction->type == INST_VALEUR_VALINDEX) {
		inst_valeur_valindex_t *instValindex = (inst_valeur_valindex_t*)instruction;
		memo_valeur_t *valeur = inst_valeur_exec(instValindex->valeur, memory, fonctions);
		memo_valeur_t *valeurAtIndex = inst_index_get_ptr_exec(instValindex->index, memory, fonctions, valeur);
		memo_valeur_t *res = memo_valeur_dup(valeurAtIndex);
		memo_valeur_free(valeur);
		return res;
	}

	return NULL;
}

void inst_valeur_free(inst_valeur_t *instruction) {

	if(instruction->type == INST_VALEUR_FONCTIONDESC) {
		inst_valeur_fonctiondesc_t *instFonctionDesc = (inst_valeur_fonctiondesc_t*)instruction;
		free(instFonctionDesc->nomFonction);
		inst_arguments_free(instFonctionDesc->arguments);
		free(instFonctionDesc->nomFonctionOutput);
		inst_code_free(instFonctionDesc->code);
		free(instFonctionDesc);
	} else if(instruction->type == INST_VALEUR_FONCTION) {
		inst_valeur_fonction_t *instFonction = (inst_valeur_fonction_t*)instruction;
		free(instFonction->nomFonction);
		inst_arguments_free(instFonction->arguments);
		free(instFonction);
	} else if(instruction->type == INST_VALEUR_VARIABLE) {
		inst_valeur_variable_t *instVariable = (inst_valeur_variable_t*)instruction;
		free(instVariable->variable);
		free(instVariable);
	} else if(instruction->type == INST_VALEUR_CHIFFRE) {
		inst_valeur_chiffre_t *instChiffre = (inst_valeur_chiffre_t*)instruction;
		free(instChiffre);
	} else if(instruction->type == INST_VALEUR_CHAINE) {
		inst_valeur_chaine_t *instChaine = (inst_valeur_chaine_t*)instruction;
		free(instChaine->chaine);
		free(instChaine);
	} else if(instruction->type == INST_VALEUR_TABLEAU) {
		inst_valeur_tableau_t *instTableau = (inst_valeur_tableau_t*)instruction;
		inst_tableau_free(instTableau->tableau);
		free(instTableau);
	} else if(instruction->type == INST_VALEUR_VALINDEX) {
		inst_valeur_valindex_t *instValindex = (inst_valeur_valindex_t*)instruction;
		inst_valeur_free(instValindex->valeur);
		inst_index_free(instValindex->index);
		free(instValindex);
	}
}