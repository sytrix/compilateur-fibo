

struct memo_valeur_t {
	int type;
};

struct memo_valeur_int_t {
	int type;
	long long valInt;
};

struct memo_valeur_list_t {
	int type;
	int length;
	struct memo_valeur_t **list;
};

struct memo_valeur_str_t {
	int type;
	char *chaine;
};

memo_valeur_t *memo_valeur_int_init(long long valeur) {
	memo_valeur_int_t *this = malloc(sizeof(memo_valeur_int_t));

	this->type = VARTYPE_INT;
	this->valInt = valeur;

	return (memo_valeur_t*)this;
}

memo_valeur_t *memo_valeur_list_init(int length) {
	memo_valeur_list_t *this = malloc(sizeof(memo_valeur_list_t));
	int i;

	this->type = VARTYPE_LIST;
	this->list = malloc(sizeof(memo_valeur_t*) * length);
	for(i = 0; i < length; i++) {
		this->list[i] = NULL;
	}
	this->length = length;

	return (memo_valeur_t*)this;
}

memo_valeur_t *memo_valeur_str_init(char *chaine) {
	memo_valeur_str_t *this = malloc(sizeof(memo_valeur_str_t));

	this->type = VARTYPE_STR;
	this->chaine = strdup(chaine);

	return (memo_valeur_t*)this;
}

memo_valeur_t *memo_valeur_dup(memo_valeur_t *todup) {
	if(todup->type == VARTYPE_INT) {
		memo_valeur_int_t *todupInt = (memo_valeur_int_t*)todup;
		return memo_valeur_int_init(todupInt->valInt);
	} else if(todup->type == VARTYPE_LIST) {
		memo_valeur_list_t *todupList = (memo_valeur_list_t*)todup;
		int length = todupList->length;
		int i;
		memo_valeur_t *res = memo_valeur_list_init(length);
		for(i = 0; i < length; i++) {
			memo_valeur_list_set(res, i, memo_valeur_dup(memo_valeur_list_get(todup, i)));
		}
		return res;
	} else if(todup->type == VARTYPE_STR) {
		memo_valeur_str_t *todupChaine = (memo_valeur_str_t*)todup;
		return memo_valeur_str_init(todupChaine->chaine);
	}

	return NULL;
}

memo_valeur_t *memo_valeur_compute(memo_valeur_t *a, memo_valeur_t *b, char sign) {
	int atype = a->type;
	int btype = b->type;
	memo_valeur_t *res = NULL;

	if(atype == VARTYPE_INT) {
		memo_valeur_int_t *aInt = (memo_valeur_int_t*)a;

		if(btype == VARTYPE_INT) {
			memo_valeur_int_t *bInt = (memo_valeur_int_t*)b;

			if(sign == '+') {
				res = memo_valeur_int_init(aInt->valInt + bInt->valInt);
			} else if(sign == '-') {
				res = memo_valeur_int_init(aInt->valInt - bInt->valInt);
			} else if(sign == '*') {
				res = memo_valeur_int_init(aInt->valInt * bInt->valInt);
			} else if(sign == '/') {
				res = memo_valeur_int_init(aInt->valInt / bInt->valInt);
			}
		}
	}

	if(res == NULL) {
		fprintf(stderr, "erreur : operation '%c' impossible entre les types %s et %s\n", 
			sign, memo_valeur_get_str_type(atype), memo_valeur_get_str_type(btype));
	}

	return res;
}

int memo_valeur_list_len(memo_valeur_t *this) {
	if(this->type == VARTYPE_INT) {
		fprintf(stderr, "erreur : operation length sur un type natif\n");
		return 0;
	} else if(this->type == VARTYPE_LIST) {
		memo_valeur_list_t *thisList = (memo_valeur_list_t*)this;
		return thisList->length;
	} else if(this->type == VARTYPE_STR) {
		fprintf(stderr, "erreur : operation len sur un type string\n");
		return 0;
	}

	fprintf(stderr, "erreur : operation length sur type non défini\n");
	return 0;
}

void memo_valeur_list_add(memo_valeur_t *this, memo_valeur_t *valeur) {
	if(this->type == VARTYPE_INT) {
		fprintf(stderr, "erreur : operation d'ajout sur un type natif\n");
		return;
	} else if(this->type == VARTYPE_LIST) {
		memo_valeur_list_t *thisList = (memo_valeur_list_t*)this;
		int length = thisList->length;
		thisList->list = realloc(thisList->list, sizeof(memo_valeur_t*) * (length + 1));
		thisList->list[length] = valeur;
		thisList->length++;
		return;
	} else if(this->type == VARTYPE_STR) {
		fprintf(stderr, "erreur : operation d'ajout sur un type string\n");
		return;
	} 

	fprintf(stderr, "erreur : operation d'ajout sur type non défini\n");
}

void memo_valeur_list_set(memo_valeur_t *this, int index, memo_valeur_t *valeur) {
	if(this->type == VARTYPE_INT) {
		fprintf(stderr, "erreur : operation modification sur un type natif\n");
		return;
	} else if(this->type == VARTYPE_LIST) {
		memo_valeur_list_t *thisList = (memo_valeur_list_t*)this;
		if(index < 0) {
			index = thisList->length - index - 1;
		}
		if(index < 0 || index >= thisList->length) {
			fprintf(stderr, "erreur : out of bound : tableau index:'%d' length:'%d'\n", index, thisList->length);
			return;
		}
		if(thisList->list[index] != NULL) {
			memo_valeur_free(thisList->list[index]);
		}
		thisList->list[index] = memo_valeur_dup(valeur);
		return;
	} else if(this->type == VARTYPE_STR) {
		fprintf(stderr, "erreur : operation modification sur un type string\n");
	} 

	fprintf(stderr, "erreur : operation modification sur type non défini\n");
}

memo_valeur_t *memo_valeur_list_get(memo_valeur_t *this, int index) {
	if(this->type == VARTYPE_INT) {
		fprintf(stderr, "erreur : operation d'acces sur un type natif\n");
		return NULL;
	} else if(this->type == VARTYPE_LIST) {
		memo_valeur_list_t *thisList = (memo_valeur_list_t*)this;
		if(index < 0) {
			index = thisList->length + index;
		}
		if(index < 0 || index >= thisList->length) {
			fprintf(stderr, "erreur : out of bound : tableau index:'%d' length:'%d'\n", index, thisList->length);
			return NULL;
		}
		return thisList->list[index];
	} else if(this->type == VARTYPE_STR) {
		fprintf(stderr, "erreur : operation d'acces sur un type string\n");
		return NULL;
	} 

	fprintf(stderr, "erreur : operation d'acces sur type non défini\n");
	return NULL;
}

void memo_valeur_list_reverse(memo_valeur_t *this) {
	if(this->type == VARTYPE_INT) {
		fprintf(stderr, "erreur : operation reverse sur un type natif\n");
		return;
	} else if(this->type == VARTYPE_LIST) {
		memo_valeur_list_t *thisList = (memo_valeur_list_t*)this;
		int length = thisList->length;
		int i;
		memo_valeur_t **tmp = malloc(length * sizeof(memo_valeur_t*));
		for(i = 0; i < length; i++) {
			tmp[i] = thisList->list[i];
		}

		for(i = 0; i < length; i++) {
			thisList->list[i] = tmp[length - i - 1];
		}

		free(tmp);
		return;
	} else if(this->type == VARTYPE_STR) {
		fprintf(stderr, "erreur : operation reverse sur un type string\n");
		return;
	} 

	fprintf(stderr, "erreur : operation reverse sur type non défini\n");
}

int memo_valeur_is_int(memo_valeur_t *this) {
	if(this->type == VARTYPE_INT) {
		return 1;
	}
	return 0;
}

int memo_valeur_is_list(memo_valeur_t *this) {
	if(this->type == VARTYPE_LIST) {
		return 1;
	}
	return 0;
}

int memo_valeur_is_str(memo_valeur_t *this) {
	if(this->type == VARTYPE_STR) {
		return 1;
	}
	return 0;
}

int memo_valeur_get_int(memo_valeur_t *this) {
	memo_valeur_int_t *thisInt = (memo_valeur_int_t*)this;
	return thisInt->valInt;
}

long long memo_valeur_get_long_int(memo_valeur_t *this) {
	memo_valeur_int_t *thisInt = (memo_valeur_int_t*)this;
	return thisInt->valInt;
}

char *memo_valeur_get_str(memo_valeur_t *this) {
	memo_valeur_str_t *thisChaine = (memo_valeur_str_t*)this;
	return thisChaine->chaine;
}

char *memo_valeur_get_str_type(int type) {
	switch(type) {
		case VARTYPE_INT:
			return "int";
		case VARTYPE_LIST:
			return "list";
		case VARTYPE_STR:
			return "string";
	}

	return "undefined";
}

char *memo_valeur_get_json_tree(memo_valeur_t *this) {
	if(this == NULL) {
		return strdup("null");
	}

	char *res = NULL;
	char *type = memo_valeur_get_str_type(this->type);

	if(this->type == VARTYPE_INT) {
		memo_valeur_int_t *thisInt = (memo_valeur_int_t*)this;
		long long valeur = thisInt->valInt;
		res = util_asprintf("{\"type\":\"%s\", \"valeur\":%ld}", type, valeur);
	} else if(this->type == VARTYPE_LIST) {
		memo_valeur_list_t *thisList = (memo_valeur_list_t*)this;
		int length = thisList->length;
		char **listJson = malloc(sizeof(char*) * length);
		memo_valeur_t **list = thisList->list;
		int needed = 0;
		int i;

		for(i = 0; i < length; i++) {
			listJson[i] = memo_valeur_get_json_tree(list[i]);
			needed += strlen(listJson[i]) + 2;
		}

		char *bufferJson = malloc(sizeof(char) * (needed + 3));

		strcpy(bufferJson, "[");

		for(i = 0; i < length; i++) {
			strcat(bufferJson, listJson[i]);
			if(i < length - 1) {
				strcat(bufferJson, ", ");
			}
			free(listJson[i]);
		}
		free(listJson);

		strcat(bufferJson, "]");

		res = util_asprintf("{\"type\":\"%s\", \"length\":%d, \"valeur\":%s}", type, length, bufferJson);
		free(bufferJson);
	} else if(this->type == VARTYPE_STR) {
		memo_valeur_str_t *thisStr = (memo_valeur_str_t*)this;
		char *chaine = thisStr->chaine;
		res = util_asprintf("{\"type\":\"%s\", \"valeur\":\"%s\"}", type, chaine);
	} else {
		res = strdup("undefined type");
	}

	return res;
}

char *memo_valeur_get_value_tree(memo_valeur_t *this) {
	if(this == NULL) {
		return strdup("null");
	}

	char *res = NULL;

	if(this->type == VARTYPE_INT) {
		memo_valeur_int_t *thisInt = (memo_valeur_int_t*)this;
		long long valeur = thisInt->valInt;
		res = util_asprintf("%ld", valeur);
	} else if(this->type == VARTYPE_LIST) {
		memo_valeur_list_t *thisList = (memo_valeur_list_t*)this;
		int length = thisList->length;
		char **listJson = malloc(sizeof(char*) * length);
		memo_valeur_t **list = thisList->list;
		int needed = 0;
		int i;

		for(i = 0; i < length; i++) {
			listJson[i] = memo_valeur_get_value_tree(list[i]);
			needed += strlen(listJson[i]) + 2;
		}

		char *bufferValue = malloc(sizeof(char) * (needed + 3));

		strcpy(bufferValue, "[");

		for(i = 0; i < length; i++) {
			strcat(bufferValue, listJson[i]);
			if(i < length - 1) {
				strcat(bufferValue, ", ");
			}
			free(listJson[i]);
		}
		free(listJson);

		strcat(bufferValue, "]");

		res = bufferValue;
	} else if(this->type == VARTYPE_STR) {
		memo_valeur_str_t *thisStr = (memo_valeur_str_t*)this;
		char *chaine = thisStr->chaine;
		res = util_asprintf("\"%s\"", chaine);
	} else {
		res = strdup("undefined type");
	}

	return res;
}

void memo_valeur_free(memo_valeur_t *this) {
	if(this->type == VARTYPE_INT) {
		memo_valeur_int_t *thisInt = (memo_valeur_int_t*)this;
		free(thisInt);
	} else if(this->type == VARTYPE_LIST) {
		memo_valeur_list_t *thisList = (memo_valeur_list_t*)this;
		int length = thisList->length;
		int i;
		for(i = 0; i < length; i++) {
			memo_valeur_free(thisList->list[i]);
		}
		free(thisList->list);
		free(thisList);
	} else if(this->type == VARTYPE_STR) {
		memo_valeur_str_t *thisStr = (memo_valeur_str_t*)this;
		free(thisStr->chaine);
		free(thisStr);
	}
}


