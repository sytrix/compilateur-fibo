
struct inst_variable_t {
	struct inst_index_t *index;
	char *nom;
};

inst_variable_t *inst_variable_init(inst_index_t *index, char *nom) {
	inst_variable_t *instruction = malloc(sizeof(inst_variable_t));

	instruction->index = index;
	instruction->nom = nom;

	return instruction;
}

char *inst_variable_get_json_tree(inst_variable_t *instruction) {
	char *res = NULL;

	if(instruction->index == NULL) {
		char *nom = instruction->nom;
		res = util_asprintf("{\"nom\":\"%s\"}", nom);
	} else {
		char *nom = instruction->nom;
		char *jsonIndex = inst_index_get_json_tree(instruction->index);
		res = util_asprintf("{\"nom\":\"%s\", \"index\":%s}", nom, jsonIndex);
		free(jsonIndex);
	}

	return res;
}

void inst_variable_affectation_exec(inst_variable_t *instruction, memo_vars_t *memory, fonction_list_t *fonctions, memo_valeur_t *valeur) {
	if(instruction->index == NULL) {
		memo_vars_put(memory, instruction->nom, valeur);
	} else {
		memo_valeur_t *valeur_ptr = memo_vars_get_ptr(memory, instruction->nom);
		if(valeur_ptr != NULL && valeur != NULL) {
			inst_index_set_ptr_exec(instruction->index, memory, fonctions, valeur_ptr, valeur, 0);
		}
	}
}

void inst_variable_free(inst_variable_t *instruction) {
	if(instruction->index != NULL) {
		inst_index_free(instruction->index);
	}
	free(instruction->nom);
	free(instruction);
}