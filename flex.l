%{
	#include <stdio.h>
	#include "y.tab.h"
%}

%%
<<EOF>>     				{								return Eof;}
^\/\/.*						{}
exit						{								return Exit;}
\"[^\"]*\"					{yylval.chaine=strdup(yytext);	return Chaine;}
(text)|(graph)				{								return TypeReturn;}
ret 						{								return Return; }
[-]?[0-9]+ 					{yylval.chiffres=atoi(yytext);	return Chiffres;}
[a-zA-Z]+ 					{yylval.nom=strdup(yytext); 	return Nom;}
[+\-\*\/]					{yylval.chr=yytext[0];			return Sign;}
[\(\)\=\>\[\]\{\};,]		{								return yytext[0];}
[ ]							;
\n 							{								return Eol;}
.							{fprintf(stderr, "Warning : incorrect char '%s'\n", yytext);}
%%

int yywrap(void) 
{
	return 1;
}



